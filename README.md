# LED-Stripes
This Project uses a Wemos to control W2812B Stripes via Artnet.

## The Software:
in order to use compile the arduino sketch you will need:
* [artnet for esp](https://github.com/nickysemenza/Artnet)
* [ArduinoJson](https://github.com/bblanchon/ArduinoJson)
* [Adafruit NeoPixel](https://github.com/adafruit/Adafruit_NeoPixel)

## The Case:
The case ist laser cuttet out of 3mm Acryl.
![case](./case.jpg)
