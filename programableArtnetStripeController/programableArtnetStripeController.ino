#include <ESP8266WiFi.h>
#include <Artnet.h>
#include <SPI.h>
#include "FS.h"

#include <Adafruit_NeoPixel.h>

#include <ArduinoJson.h>

#define DMX_ID 32
#define PIN D4

Artnet artnet;
Adafruit_NeoPixel *strip;

StaticJsonBuffer<2000> jsonBuffer;

int initState = 0;//0 startup; 1; wlan searching 2 wlan up; 3artnet up

const char* ssid;
const char* pw;
bool multiLedMode = false;
int numLEDs = 1;
int ledType = 0; //0--> rgb; 1--> rgbw
int dmxChannel = 0;
int dmxUniverse = 0;

void setup()
{

  SPIFFS.begin();
  Serial.begin(9600);
}

void colorWipe(uint32_t c, uint8_t wait) {
  for (uint16_t i = 0; i < strip->numPixels(); i++) {
    strip->setPixelColor(i, c);
    strip->show();
    delay(wait);
  }
}

void loop()
{

  if (initState == 0) {
    //load config:
    File f = SPIFFS.open("/config.txt", "r");
    JsonObject& root = jsonBuffer.parseObject(f.readString());
    // Serial.println(f.readString());
    f.close();
    if (!root.success()) {
      Serial.println("json error");
    }
    else {
      if (root.containsKey("ssid"))
        ssid = root["ssid"];
      if (root.containsKey("pw"))
        pw = root["pw"];
      if (root.containsKey("mode"))
        multiLedMode = (strcmp(root["mode"], "multi-stripe") == 0 );
      if (root.containsKey("numLed"))
        numLEDs = root["numLed"];
      if (root.containsKey("channel"))
        dmxChannel = root["channel"];
      if (root.containsKey("universe"))
        dmxUniverse = root["universe"];
      if (root.containsKey("ledType"))
        ledType = root["ledType"] == "rgbw" ? 1 : 0;


      /*Serial.print("numLed");
        Serial.println(numLEDs);

        Serial.print("ssid: ");
        Serial.println(ssid);

        Serial.print("pw: ");
        Serial.println(pw);*/

      WiFi.begin(ssid, pw);
      initState++;
    }
  }
  else if (initState == 1) {
    // Wait for connection
    if (WiFi.status() == WL_CONNECTED) {
      Serial.print("IP address: ");
      Serial.println(WiFi.localIP());
      initState++;
    }
  }
  else if (initState == 2) {
    artnet.begin();
    byte brodcast[4];
    brodcast[0] = WiFi.localIP()[0];
    brodcast[1] = WiFi.localIP()[1];
    brodcast[2] = WiFi.localIP()[2];
    brodcast[3] = 255;
    artnet.setBroadcast(brodcast);
    artnet.setUniverseA(dmxUniverse);

    strip = new Adafruit_NeoPixel(numLEDs, PIN, NEO_GRB + NEO_KHZ800);

    strip->begin();
    strip->show();

    colorWipe(strip->Color(0, 255, 0), 50);

    Serial.println("init finished");
    initState++;
  }
  else {
    uint16_t r = artnet.read();
    if (r == ART_DMX)
    {
      if (artnet.getUniverse() == dmxUniverse) {
        if (multiLedMode) {
          int numLedCol = 3;
          for (uint16_t i = 0; i < strip->numPixels(); i++) {
            int x = dmxChannel + i * numLedCol;
            strip->setPixelColor(i, strip->Color(artnet.getDmxFrame()[x],
                                                 artnet.getDmxFrame()[x + 1],
                                                 artnet.getDmxFrame()[x + 2]));
          }
          strip->show();
        }
        else {
          colorWipe(strip->Color(artnet.getDmxFrame()[dmxChannel],
                                 artnet.getDmxFrame()[dmxChannel + 1],
                                 artnet.getDmxFrame()[dmxChannel + 2]), 0);
        }
      }
    }
  }

  if (Serial.available() > 0) {
    String read = Serial.readString();

    //write config
    File f = SPIFFS.open("/config.txt", "w");
    Serial.println("ok");
    f.println(read);
    f.flush();
    f.close();
    WiFi.disconnect();
    delete strip;
    initState = 0; //reinit
  }
}

